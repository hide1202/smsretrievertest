package com.nhnent.smsretrievertest

import android.os.Bundle
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient


class MyGoogleApiClientCallbacks : GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    override fun onConnected(p0: Bundle?) {
        Log.i(TAG, "onConnected")
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.i(TAG, "onConnectionSuspended")
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.i(TAG, "onConnectionFailed")
    }

    companion object {
        val TAG: String = MyGoogleApiClientCallbacks::class.java.simpleName
    }
}