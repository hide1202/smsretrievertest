package com.nhnent.smsretrievertest

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import com.google.android.gms.auth.api.phone.SmsRetriever

class MySMSBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION == intent.action) {
            val localIntent = Intent(MainActivity.ACTION_LOCAL_RECEIVER)
            localIntent.putExtras(intent.extras ?: Bundle())
            LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent)
        }
    }
}