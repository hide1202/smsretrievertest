package com.nhnent.smsretrievertest

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig
import com.google.android.gms.auth.api.credentials.HintRequest
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.auth.api.phone.SmsRetrieverClient
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Status


/*
    SMS Content :
        <#> 인증번호 [871537] 입니다
        RCQ4EwgZBZ1
*/
class MainActivity : AppCompatActivity() {

    lateinit var apiClient: GoogleApiClient
    lateinit var callbacks: MyGoogleApiClientCallbacks
    lateinit var receiver: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val signatures = AppSignatureHelper(this).appSignatures
        signatures.forEachIndexed { i, s ->
            Log.i(TAG, "Signature[$i] : $s")
        }

        callbacks = MyGoogleApiClientCallbacks()
        apiClient = GoogleApiClient.Builder(this).addConnectionCallbacks(callbacks)
            .enableAutoManage(this, callbacks)
            .addApi(Auth.CREDENTIALS_API)
            .build()

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == MainActivity.ACTION_LOCAL_RECEIVER) {
                    val extras = intent.extras
                    val status = extras!!.get(SmsRetriever.EXTRA_STATUS) as Status

                    when (status.statusCode) {
                        CommonStatusCodes.SUCCESS -> {
                            val message = extras.get(SmsRetriever.EXTRA_SMS_MESSAGE) as String
                            AlertDialog.Builder(this@MainActivity)
                                .setTitle("결과")
                                .setMessage("[SMS 수신]\n$message")
                                .setPositiveButton("확인") { dialog, which -> dialog.dismiss() }
                                .create()
                                .show()
                        }
                        CommonStatusCodes.TIMEOUT -> {
                            AlertDialog.Builder(this@MainActivity)
                                .setTitle("결과")
                                .setMessage("타임아웃으로 인한 실패!")
                                .setPositiveButton("확인") { dialog, which -> dialog.dismiss() }
                                .create()
                                .show()
                        }
                    }
                }
            }
        }
    }

    fun registerSmsReceiver(view: View): Unit {
        val client: SmsRetrieverClient = SmsRetriever.getClient(this /* context */)

        val task = client.startSmsRetriever()

        task.addOnSuccessListener {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver)
            LocalBroadcastManager.getInstance(this).registerReceiver(receiver, IntentFilter(ACTION_LOCAL_RECEIVER))

            AlertDialog.Builder(this)
                .setTitle("성공")
                .setMessage("SMS Retriever 등록 성공")
                .setPositiveButton("확인") { dialog, which -> dialog.dismiss() }
                .create()
                .show()
        }

        task.addOnFailureListener {
            AlertDialog.Builder(this)
                .setTitle("실패")
                .setMessage("SMS Retriever 등록 실패")
                .setPositiveButton("확인") { dialog, which -> dialog.dismiss() }
                .create()
                .show()
        }
    }

    fun requestHint(view: View): Unit {
        val hintRequest = HintRequest.Builder()
            .setHintPickerConfig(
                CredentialPickerConfig.Builder()
                    .setShowCancelButton(true)
                    .build()
            )
            .setPhoneNumberIdentifierSupported(true)
            .build()

        val intent = Auth.CredentialsApi.getHintPickerIntent(
            apiClient, hintRequest
        )
        startIntentSenderForResult(
            intent.intentSender,
            REQUEST_RESOLVE_HINT, null, 0, 0, 0
        )
    }

    // Obtain the phone number from the result
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_RESOLVE_HINT) {
            if (resultCode == Activity.RESULT_OK) {
                val credential: Credential = data!!.getParcelableExtra(Credential.EXTRA_KEY)
                AlertDialog.Builder(this)
                    .setTitle("성공")
                    .setMessage("힌트 요청 성공 : ${credential.id}")
                    .setPositiveButton("확인") { dialog, _ -> dialog.dismiss() }
                    .create()
                    .show()
            } else {
                AlertDialog.Builder(this)
                    .setTitle("실패")
                    .setMessage("힌트 요청 실패 : $resultCode")
                    .setPositiveButton("확인") { dialog, _ -> dialog.dismiss() }
                    .create()
                    .show()
            }
        }
    }


    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver)
        super.onDestroy()
    }

    companion object {
        val TAG: String = MainActivity::class.java.simpleName
        const val ACTION_LOCAL_RECEIVER: String = "receive.sms.result"
        const val REQUEST_RESOLVE_HINT: Int = 10101
    }
}